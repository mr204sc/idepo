<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'idepo_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1[Hm{N#`Oh=^Ay=o/PMJ2@sN%`9G# WAjWvZ)I|sRZs;T~J}D[vsL,$d&R3~motS' );
define( 'SECURE_AUTH_KEY',  '# ~7w_!p+(}*{Bk_^O,rp*8k~VXW^FU5PyBG4Vs!AU;Mv_*QMfWfol& 1.1bXq v' );
define( 'LOGGED_IN_KEY',    '1S6[!Yk%J#zB{:4ru>!tLlVVWJ$[=cXbZie7<Qb}_E]d4q<N_w*9e<L#:P?){@oo' );
define( 'NONCE_KEY',        'ZWw![N)%/d}K1R?U-![e.0Q5FMN=6ziJUV@3~I#vj51*vTvQDR*sK0B]3gTvS/d,' );
define( 'AUTH_SALT',        'tHK:.6t ^FAL0TpA[2N`d%rr^vBXHF[e]0sTs`OTLp/ U@#),+Sr)+;I3g9l%uPd' );
define( 'SECURE_AUTH_SALT', 'gh%8JGFF!BmM@cWSusrh}y4Hp+>0p0,6s=Be)Ux-zS;ySnH@2}YwQ%(1|p$X<Yc ' );
define( 'LOGGED_IN_SALT',   'xy./tYw0Z-tbkdt[9hr<LwuB(0=F gTJYSN6+23|_J,)N nqSKh2l9,iS%/6E_CY' );
define( 'NONCE_SALT',       'jue+KGab1/cY@y3J<)Nvg|:7}ja A^P|tE([~>f7[QNQ:F.;t~r1[-r{ID=c9q3a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
