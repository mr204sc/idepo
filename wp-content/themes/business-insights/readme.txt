=== Business Insights ===

Contributors: ThemeInWP

Requires at least: 4.0
Tested up to: 4.9.4
Stable tag: 1.0.3
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A clean, elegent and professional WordPress Blog Theme.

== Description ==

Business Insights is a free responsive very stylish, elegant and powerful multipurpose WordPress corporate theme that allows you to create stunning business, corporate, personal as well as agency websites. It has been engineered to be easy to use and fast. It assist you to create flexible, clean and modern multipurpose responsive WordPress theme that is developed to build a wide variety of business websites, business agencies, creative agencies, digital agencies, corporate houses, blog and other creative websites. Easy installation and fully optmized codes brings you absolutely no coding knowledge required you just need a few clicks in order to achieve professional websites. 


Have a support or pre-sale question? Feel free to ask it in comments.

Have an idea for the next theme update? We?re always happy to hear your thoughts on new features.

== Copyright ==

Business Insights WordPress Theme, Copyright 2018 ThemeInWP
Business Insights is distributed under the terms of the GNU GPL

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Credits ==

Underscores:
Author: 2012-2015 Automattic
Source: http://underscores.me
License: GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

normalize:
Author: 2012-2015 Nicolas Gallagher and Jonathan Neal
Source: http://necolas.github.io/normalize.css
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

html5shiv:
Author: 2014 Alexander Farkas (aFarkas)
Source: https://github.com/afarkas/html5shiv
License: [MIT/GPL2 Licensed](http://opensource.org/licenses/MIT)

jquery.easing:
Author: 2008 George McGinley Smith
Source: https://raw.github.com/gdsmith/jquery-easing/master
License: [MIT](http://opensource.org/licenses/MIT)

Bootstrap:
Author: Twitter
Source: http://getbootstrap.com
License: Licensed under the MIT license

BreadcrumbTrail:
Author: Justin Tadlock
Source: http://themehybrid.com/plugins/breadcrumb-trail
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Elegant Icon:
Author: Elegant Themes
Source: https://github.com/josephnle/elegant-icons and https://www.elegantthemes.com/blog/resources/elegant-icon-font
License: Licensed under the GPL v2.0 and MIT license.

Owl carousel:
Author: Bartosz Wojciechowski
Source: https://github.com/OwlFonk/OwlCarousel
License: The MIT License (MIT)

jquery-match-height:
Author: liabru
Source: http://brm.io/jquery-match-height/
License: Licensed under the MIT license

TGM-Plugin-Activation:
Author: Thomas Griffin (thomasgriffinmedia.com)
URL:  https://github.com/TGMPA/TGM-Plugin-Activation
License: GNU General Public License, version 2

== Image Used ==
https://www.pexels.com/photo/group-hand-fist-bump-1068523/
https://www.pexels.com/photo/people-talking-seating-on-chair-705674/
All are Licensed under CC0


== Google Fonts ==
Open Sans
Source: https://fonts.google.com/specimen/Open+Sans
License: Apache License, Version 2.0

Roboto Condensed
Source: https://fonts.google.com/specimen/Roboto+Condensed
License: Apache License, Version 2.0

== Changelog ==

= 1.0.3 =
= Compatible with WpForms added=

= 1.0.2 =
= Fixed issues on user feedbaks=

= 1.0.1 =
= One Click Demo Import Maintained=


= 1.0.0 =
= Initial release=

== Note ==
The top menu will not display any drop down items.


