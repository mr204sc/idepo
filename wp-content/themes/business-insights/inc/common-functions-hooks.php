<?php
if (!function_exists('business_insights_the_custom_logo')):
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since Business Insights 1.0.0
 */
function business_insights_the_custom_logo() {
	if (function_exists('the_custom_logo')) {
		the_custom_logo();
	}
}
endif;

if (!function_exists('business_insights_body_class')):

/**
 * body class.
 *
 * @since 1.0.0
 */
function business_insights_body_class($business_insights_body_class) {
	global $post;
	$global_layout       = business_insights_get_option('global_layout');
	$input               = '';
	$home_content_status = business_insights_get_option('home_page_content_status');
	if (1 != $home_content_status) {
		$input = 'home-content-not-enabled';
	}
	// Check if single.
	if ($post && is_singular()) {
		$post_options = get_post_meta($post->ID, 'business-insights-meta-select-layout', true);
		if (empty($post_options)) {
			$global_layout = esc_attr(business_insights_get_option('global_layout'));
		} else {
			$global_layout = esc_attr($post_options);
		}
	}
	if ($global_layout == 'left-sidebar') {
		$business_insights_body_class[] = 'left-sidebar '.esc_attr($input);
	} elseif ($global_layout == 'no-sidebar') {
		$business_insights_body_class[] = 'no-sidebar '.esc_attr($input);
	} else {
		$business_insights_body_class[] = 'right-sidebar '.esc_attr($input);

	}
	return $business_insights_body_class;
}
endif;

add_action('body_class', 'business_insights_body_class');

add_action('business_insights_action_sidebar', 'business_insights_add_sidebar');

/**
 * Returns word count of the sentences.
 *
 * @since Business Insights 1.0.0
 */
if (!function_exists('business_insights_words_count')):
function business_insights_words_count($length = 25, $business_insights_content = null) {
	$length          = absint($length);
	$source_content  = preg_replace('`\[[^\]]*\]`', '', $business_insights_content);
	$trimmed_content = wp_trim_words($source_content, $length, '');
	return $trimmed_content;
}
endif;

if (!function_exists('business_insights_simple_breadcrumb')):

/**
 * Simple breadcrumb.
 *
 * @since 1.0.0
 */
function business_insights_simple_breadcrumb() {

	if (!function_exists('breadcrumb_trail')) {

		require_once get_template_directory().'/assets/libraries/breadcrumbs/breadcrumbs.php';
	}

	$breadcrumb_args = array(
		'container'   => 'div',
		'show_browse' => false,
	);
	breadcrumb_trail($breadcrumb_args);

}

endif;



if ( ! function_exists( 'business_insights_ajax_pagination' ) ) :
    /**
     * Outputs the required structure for ajax loading posts on scroll and click
     *
     * @since 1.0.0
     * @param $type string Ajax Load Type
     */
    function business_insights_ajax_pagination($type) {
        ?>
        <div class="load-more-posts" data-load-type="<?php echo esc_attr($type);?>">
            <a href="#" class="btn-link btn-link-load">
                <span class="ajax-loader"></span>
                <?php _e('Load More Posts', 'business-insights')?>
                <i class="ion-ios-arrow-right"></i>
            </a>
        </div>
        <?php
    }
endif;

if ( ! function_exists( 'business_insights_load_more' ) ) :
    /**
     * Ajax Load posts Callback.
     *
     * @since 1.0.0
     *
     */
    function business_insights_load_more() {

        check_ajax_referer( 'business-insights-load-more-nonce', 'nonce' );

        $output['more_post'] = false;
        $output['content'] = array();

        $args['post_type'] = ( isset( $_GET['post_type']) && !empty($_GET['post_type'] ) ) ? esc_attr( $_GET['post_type'] ) : 'post';
        $args['post_status'] = 'publish';
        $args['paged'] = (int) esc_attr( $_GET['page'] );

        if( isset( $_GET['cat'] ) && isset( $_GET['taxonomy'] ) ){
            $args['tax_query'] = array(
                array(
                    'taxonomy' => esc_attr($_GET['taxonomy']),
                    'field'    => 'slug',
                    'terms'    => array(esc_attr($_GET['cat'])),
                ),
            );
        }

        if( isset($_GET['search']) ){
            $args['s'] = esc_attr( $_GET['search'] );
        }

        if( isset($_GET['author']) ){
            $args['author_name'] = esc_attr( $_GET['author'] );
        }

        if( isset($_GET['year']) || isset($_GET['month']) || isset($_GET['day']) ){

            $date_arr = array();

            if( !empty($_GET['year']) ){
                $date_arr['year'] = (int) esc_attr($_GET['year']);
            }
            if( !empty($_GET['month']) ){
                $date_arr['month'] = (int) esc_attr($_GET['month']);
            }
            if( !empty($_GET['day']) ){
                $date_arr['day'] = (int) esc_attr($_GET['day']);
            }

            if( !empty($date_arr) ){
                $args['date_query'] = array($date_arr);
            }
        }

        $loop = new WP_Query( $args );
        if($loop->max_num_pages > $args['paged']){
            $output['more_post'] = true;
        }
        if ( $loop->have_posts() ):
            while ( $loop->have_posts() ): $loop->the_post();
                ob_start();
                get_template_part('template-parts/content', get_post_format());
                $output['content'][] = ob_get_clean();
            endwhile;wp_reset_postdata();
            wp_send_json_success($output);
        else:
            $output['more_post'] = false;
            wp_send_json_error($output);
        endif;
        wp_die();
    }
endif;
add_action( 'wp_ajax_business_insights_load_more', 'business_insights_load_more' );
add_action( 'wp_ajax_nopriv_business_insights_load_more', 'business_insights_load_more' );


if (!function_exists('business_insights_custom_posts_navigation')):
/**
 * Posts navigation.
 *
 * @since 1.0.0
 */
function business_insights_custom_posts_navigation() {

	$pagination_type = business_insights_get_option('pagination_type');

	switch ($pagination_type) {

		case 'default':
			the_posts_navigation();
			break;

		case 'numeric':
			the_posts_pagination();
			break;

        case 'infinite_scroll_load':
            business_insights_ajax_pagination('scroll');
            break;

		default:
			break;
	}

}
endif;

add_action('business_insights_action_posts_navigation', 'business_insights_custom_posts_navigation');

if (!function_exists('business_insights_excerpt_length') && !is_admin()):

/**
 * Excerpt length
 *
 * @since  Business Insights 1.0.0
 *
 * @param null
 * @return int
 */
function business_insights_excerpt_length($length) {
	$excerpt_length = business_insights_get_option('excerpt_length_global');
	if (empty($excerpt_length)) {
		$excerpt_length = $length;
	}
	return absint($excerpt_length);

}

add_filter('excerpt_length', 'business_insights_excerpt_length', 999);
endif;

if (!function_exists('business_insights_excerpt_more') && !is_admin()):

/**
 * Implement read more in excerpt.
 *
 * @since 1.0.0
 *
 * @param string $more The string shown within the more link.
 * @return string The excerpt.
 */
function business_insights_excerpt_more($more) {

	$flag_apply_excerpt_read_more = apply_filters('business_insights_filter_excerpt_read_more', true);
	if (true !== $flag_apply_excerpt_read_more) {
		return $more;
	}

	$output         = $more;
	$read_more_text = esc_html(business_insights_get_option('read_more_button_text'));
	if (!empty($read_more_text)) {
		$output = ' <a href="'.esc_url(get_permalink()).'" class="btn-link btn-link-primary">'.esc_html($read_more_text).'<i class="ion-ios-arrow-right"></i>'.'</a>';
		$output = apply_filters('business_insights_filter_read_more_link', $output);
	}
	return $output;

}

add_filter('excerpt_more', 'business_insights_excerpt_more');
endif;

if (!function_exists('business_insights_get_link_url')):

/**
 * Return the post URL.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since 1.0.0
 *
 * @return string The Link format URL.
 */
function business_insights_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content($content);

	return ($has_url)?$has_url:apply_filters('the_permalink', get_permalink());
}

endif;

if (!function_exists('business_insights_fonts_url')):

/**
 * Return fonts URL.
 *
 * @since 1.0.0
 * @return string Fonts URL.
 */
function business_insights_fonts_url() {
	$fonts_url = '';
	$fonts     = array();

	$business_insights_primary_font   = business_insights_get_option('primary_font');
	$business_insights_secondary_font = business_insights_get_option('secondary_font');

	$business_insights_fonts   = array();
	$business_insights_fonts[] = $business_insights_primary_font;
	$business_insights_fonts[] = $business_insights_secondary_font;

	$business_insights_fonts_stylesheet = '//fonts.googleapis.com/css?family=';

	$i = 0;
	for ($i = 0; $i < count($business_insights_fonts); $i++) {

		if ('off' !== sprintf(_x('on', '%s font: on or off', 'business-insights'), $business_insights_fonts[$i])) {
			$fonts[] = $business_insights_fonts[$i];
		}

	}

	if ($fonts) {
		$fonts_url = add_query_arg(array(
				'family' => urldecode(implode('|', $fonts)),
			), 'https://fonts.googleapis.com/css');
	}

	return $fonts_url;
}

endif;

/*Recomended plugin*/
if (!function_exists('business_insights_recommended_plugins')):

/**
 * Recommended plugins
 *
 */
function business_insights_recommended_plugins() {
	$business_insights_plugins = array(
		array(
			'name'     => __('One Click Demo Import', 'business-insights'),
			'slug'     => 'one-click-demo-import',
			'required' => false,
		),
        array(
            'name'     => __('Elementor Page Builder', 'business-insights'),
            'slug'     => 'elementor',
            'required' => false,
        ),
        array(
            'name'     => __('Contact Form by WPForms', 'business-insights'),
            'slug'     => 'wpforms-lite',
            'required' => false,
        ),
	);
	$business_insights_plugins_config = array(
		'dismissable' => true,
	);

	tgmpa($business_insights_plugins, $business_insights_plugins_config);
}
endif;
add_action('tgmpa_register', 'business_insights_recommended_plugins');


function business_insights_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'business_insights_archive_title' );


function business_insights_check_other_plugin() {
    // check for plugin using plugin name
    if (is_plugin_active('one-click-demo-import/one-click-demo-import.php')) {
        // Disable PT branding.
        add_filter('pt-ocdi/disable_pt_branding', '__return_true');
        //plugin is activated
        function ocdi_after_import_setup() {
            // Assign menus to their locations.
            $main_menu   = get_term_by('name', 'Primary Menu', 'nav_menu');
            $social_menu = get_term_by('name', 'Social menu', 'nav_menu');

            set_theme_mod('nav_menu_locations', array(
                    'primary' => $main_menu->term_id,
                    'social'  => $social_menu->term_id,
                )
            );

            // Assign front page and posts page (blog page).
            $front_page_id = get_page_by_title('Homepage');
            $blog_page_id  = get_page_by_title('Blog');

            update_option('show_on_front', 'page');
            update_option('page_on_front', $front_page_id->ID);
            update_option('page_for_posts', $blog_page_id->ID);

        }
        add_action('pt-ocdi/after_import', 'ocdi_after_import_setup');
    }
}
add_action('admin_init', 'business_insights_check_other_plugin');

 /**
 * Set the WPForms ShareASale ID.
 *
 * @param string $shareasale_id The the default Shareasale ID.
 *
 * @return string $shareasale_id
 */
function business_insights_wpforms_shareasale_id( $shareasale_id ) {
    
    // If this WordPress installation already has an WPForms Shareasale ID
    // specified, use that.
    if ( ! empty( $shareasale_id ) ) {
        return $shareasale_id;
    }
    
    // Define the Shareasale ID to use.
    $shareasale_id = '1821764';
    
    // This WordPress installation doesn't have an Shareasale ID specified, so 
    // set the default ID in the WordPress options and use that.
    update_option( 'wpforms_shareasale_id', $shareasale_id );
    
    // Return the Shareasale ID.
    return $shareasale_id;
}
add_filter( 'wpforms_shareasale_id', 'business_insights_wpforms_shareasale_id' );